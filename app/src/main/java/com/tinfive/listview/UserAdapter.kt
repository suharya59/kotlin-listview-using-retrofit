package com.tinfive.listview

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.tinfive.listview.model.DataItem

class UserAdapter(
    val users: List<DataItem?>?
) :
    RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.user_item_layout, parent, false) as View

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        //Elvis Operator == Ternary Kotlin
        return users?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_firstName.text = users!![position]!!.firstName
        val lastName = " ${users[position]!!.lastName}"
        holder.tv_lastName.text = lastName

        Picasso.get()
            .load(users[position]!!.avatar)
            .placeholder(R.mipmap.ic_launcher)
            .into(holder.iv_avatar)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_firstName = view.findViewById<TextView>(R.id.tv_firstName)
        val tv_lastName = view.findViewById<TextView>(R.id.tv_lastName)
        val iv_avatar = view.findViewById<ImageView>(R.id.iv_avatar)
    }
}
