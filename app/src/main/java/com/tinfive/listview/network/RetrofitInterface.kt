package com.tinfive.listview.network

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitInterface {
    // URL API
    //https://reqres.in/api/users?per_page=12

    @GET("users")
    fun getUsers(
        @Query("per_page") perPage: String
    ): Call<JsonObject>
}