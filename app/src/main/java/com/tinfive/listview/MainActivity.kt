package com.tinfive.listview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.tinfive.listview.model.DataItem
import com.tinfive.listview.model.UsersResponse
import com.tinfive.listview.network.RetrofitService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // ? => Null label
        var a : String? = "Test"
        a = null
        getUser()
    }

    private fun getUser() {
        val service = RetrofitService()
        service.getUser(object : Callback<JsonObject> {

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.isSuccessful) {
                    val data = response.body()
                    val userData = Gson().fromJson(data.toString(), UsersResponse::class.java)
                    initView(userData.data)
                    Log.d("tag", "responsennya : ${data}")
                }
            }

            override fun onFailure(call: Call<JsonObject>, error: Throwable) {
                Log.e("tag", "errornya ${error.message}")
            }
        })
    }

    private fun initView(data: List<DataItem?>?) {
        // setDefaultView(firstNames,lastNames, avatars)
        val userListView = findViewById<RecyclerView>(R.id.user_item_layout)
        userListView.adapter = UserAdapter(data)
        userListView.layoutManager = LinearLayoutManager(this)
    }
}
